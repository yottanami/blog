---
layout: post
title: Perfect Terminal Multiplexer
date: '2013-11-18T12:27:00+03:30'
tags:
- gnu
- terminal
- linux
author: yottanami
category: linux
---
If you know GNU Screen or you use it I suggest you try Tmux, It is a perfetc terminal multiplexer that lets you run numerous TTY’s in the same terminal window.

[Tmux Home Page](http://tmux.sourceforge.net/)
