---
layout: post
title: Counting code lines
date: '2013-11-25T12:02:52+03:30'
tags:
- Programming
author: yottanami
category: linux
---
If you want to count lines of code of a project one of the best way is wc tool in GNU/Linux . but there is many other tools that will help you, Here I will introduce some of them:

http://en.wikipedia.org/wiki/Wc_(Unix)
http://www.dwheeler.com/sloccount/
http://metrics.sourceforge.net/
https://github.com/craigbarnes/tally
http://www.codeanalyzer.teel.ws/
