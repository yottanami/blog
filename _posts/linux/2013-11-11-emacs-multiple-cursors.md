---
layout: post
title: Emacs multiple-cursors
date: '2013-11-11T13:13:11+03:30'
tags:
- emacs
- gnu
- editor
author: yottanami
category: linux
---
Emacs multiple-cursors helps you select and change several same strings together.

More Information :
[Emacs multiple-cursors of Github](https://github.com/magnars/multiple-cursors.el()
[Emacs multiple-cursors tutorial on Emacs Rocks!](http://emacsrocks.com/e13.html)
